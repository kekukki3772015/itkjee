package jpa;

import org.springframework.context.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) throws Exception {

        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(Config.class);

        PersonDao dao = ctx.getBean(PersonDao.class);

        dao.save(new Person("John"));

        System.out.println(dao.findAllPersons());

        ((ConfigurableApplicationContext) ctx).close();
    }
}