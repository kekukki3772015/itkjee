package jdbc;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(basePackages = {"jdbc"})
@PropertySource("classpath:/application.properties")
public class Config {

    @Resource
    public Environment env;

    @Bean(name = "hsqlDataSource")
    public DataSource hsqlDataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.hsqldb.jdbcDriver");
        ds.setUsername("sa");
        ds.setPassword("");
        ds.setUrl(env.getProperty("db.url"));
        return ds;
    }

    @Bean(name = "oracleDataSource")
    public DataSource oracleDataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.hsqldb.jdbcDriver");
        ds.setUrl("jdbc:oracle:thin:@localhost:1521:dbname");
        ds.setUsername("scott");
        ds.setPassword("tiger");
        return ds;
    }

    @Bean
    public JdbcTemplate getTemplate() {
        return new JdbcTemplate(hsqlDataSource());
    }
}