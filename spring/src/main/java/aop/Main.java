package aop;

import org.springframework.context.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) throws Exception {

        ApplicationContext ctx =
                  new AnnotationConfigApplicationContext(Config.class);

        CustomerService service = ctx.getBean(CustomerService.class);

        System.out.println(service.getClass());

        service.addCustomer("John");

        ((ConfigurableApplicationContext) ctx).close();
    }
}