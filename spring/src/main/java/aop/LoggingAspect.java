package aop;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    @Around("execution(* aop.CustomerService.*(..))")
    public void logAround(ProceedingJoinPoint joinPoint) throws Throwable {

        System.out.println("method : " + joinPoint.getSignature().getName());
        System.out.println("arguments : " + Arrays.toString(joinPoint.getArgs()));

        System.out.println("Around before is running!");
        joinPoint.proceed();
        System.out.println("Around after is running!");
    }

}