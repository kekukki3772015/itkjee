package cache;

import org.springframework.context.*;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(Config.class);

        MessageRepository repo = ctx.getBean(MessageRepository.class);

        System.out.println(repo.getTranslation("hello.world"));
        System.out.println(repo.getTranslation("hello.world"));

        repo.save(new Message("key1", "Hello"));

        System.out.println(repo.getMessage("key1"));
        System.out.println(repo.getMessage("key1"));

        repo.save(new Message("key1", "World"));

        System.out.println(repo.getMessage("key1"));
        System.out.println(repo.getMessage("key1"));

        ((ConfigurableApplicationContext) ctx).close();
    }
}