package cache;

public class Message {

    private final String key;
    private final String content;

    public Message(String key, String content) {
        this.key = key;
        this.content = content;
    }

    public String getKey() {
        return key;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "Message [key=" + key + ", content=" + content + "]";
    }

}
