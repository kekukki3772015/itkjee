package xmlconf;

import org.springframework.context.*;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public static void main(String[] args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext(
                "xmlconf/xml-conf.xml");

        PersonDao personDao = ctx.getBean(PersonDao.class);

        System.out.println(personDao.getPersonName(null));

        TransferService transferService = ctx.getBean(TransferService.class);

        System.out.println(transferService.bankService);

        ((ConfigurableApplicationContext) ctx).close();
    }
}