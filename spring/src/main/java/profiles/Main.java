package profiles;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

    public static void main(String[] args) throws Exception {

        AnnotationConfigApplicationContext ctx =
                                new AnnotationConfigApplicationContext();

        ctx.getEnvironment().setActiveProfiles("test"); // production
        ctx.register(CommonConfig.class);
        ctx.refresh();

        PersonDao dao = ctx.getBean(PersonDao.class);

        System.out.println(dao.getPersonName(null));

        ctx.close();
    }
}