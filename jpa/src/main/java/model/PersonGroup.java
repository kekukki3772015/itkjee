package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@NamedQuery(name=PersonGroup.FIND_BY_NAME,
           query="SELECT g FROM PersonGroup g WHERE g.name = :name")
public class PersonGroup extends AbstractEntity {

    public static final String FIND_BY_NAME = "PersonGroup.findByName";

    public PersonGroup() {}

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    private List<Person> members = new ArrayList<Person>();

    public PersonGroup(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public void addMember(Person person) {
        person.setGroup(this);
        members.add(person);
    }

    public List<Person> getMembers() {
        return members;
    }

    public void setMembers(List<Person> members) {
        this.members = members;
    }
}
