package example;

import java.util.*;

import javax.persistence.*;

import model.*;

import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class Main {

    private static EntityManagerFactory emf;

    @BeforeClass
    public static void createFactory() {
        emf = Persistence.createEntityManagerFactory("my-hsql-unit");
    }

    @AfterClass
    public static void closeFactory() {
        emf.close();
    }

    @Before
    public void cleanDatabase() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.createNamedQuery("Admin.truncateTables").executeUpdate();
        em.createNamedQuery("Admin.resetSequence").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    @Test
    public void insertPerson() {
        assertThat(findPersonByName("Jack"), is(nullValue()));

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Person person = new Person("Jack");
        em.persist(person);

        em.getTransaction().commit();
        em.close();

        assertThat(findPersonByName("Jack"), is(not(nullValue())));
    }

    @Test
    public void updatePerson() {
        Person person = insertPerson("Bill");

        assertThat(findPersonByName("Jill"), is(nullValue()));

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        person.setName("Jill");
        em.merge(person);

        em.getTransaction().commit();
        em.close();

        assertThat(findPersonByName("Jill"), is(not(nullValue())));
    }

    @Test
    public void deletePerson() {
        Person bill = insertPerson("Bill");
        assertThat(findPersonByName("Bill"), is(not(nullValue())));

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Person found = em.find(Person.class, bill.getId());
        if (found != null) em.remove(found);

        em.getTransaction().commit();
        em.close();

        assertThat(findPersonByName("Bill"), is(nullValue()));
    }

    @Test
    public void addAddresses() {
        Person person = new Person("Jack");
        person.setAddresses(Arrays.asList(
                new Address("Kase 1"), new Address("Kuuse 2")));

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(person);

        em.getTransaction().commit();
        em.close();

        assertThat(findPersonByName("Jack")
                .getAddresses().size(), is(2));
    }

    @Test
    public void deleteAllPersons() {
        insertPerson("Jack");
        insertPerson("Jill");
        assertThat(findAllPersons().size(), is(2));

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Query query = em.createQuery("delete from Person");
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();

        assertThat(findAllPersons().size(), is(0));
    }

    @Test
    public void queryWithParameters() {
        insertPerson("Jack");

        EntityManager em = emf.createEntityManager();
        TypedQuery<Person> query = em.createQuery(
                "select e from Person e where e.name = :name", Person.class);
        query.setParameter("name", "Jack");
        Person person = query.getSingleResult();
        em.close();

        assertThat(person.getName(), is("Jack"));
    }

    @Test
    public void namedQuery() {
        insertPerson("Jack");

        EntityManager em = emf.createEntityManager();

        TypedQuery<Person> namedQuery = em.createNamedQuery(
                Person.FIND_ALL, Person.class);
        List<Person> persons = namedQuery.getResultList();
        em.close();

        assertThat(persons.get(0).getName(), is("Jack"));
    }

    @Test
    public void navigateManyToOne() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        PersonGroup group = new PersonGroup("G1");
        group.addMember(new Person("Jill"));
        group.addMember(new Person("Jack"));
        em.persist(group);

        em.getTransaction().commit();
        em.close();

        assertThat(findPersonByName("Jill")
                .getGroup().getName(), is("G1"));
    }

    @Test
    public void modifyCollection() {
        insertPerson("Jill", Arrays.asList(
                new Address("Kase 1"), new Address("Kuuse 2")));

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Person jill = findPersonByName("Jill");
        jill.getAddresses().remove(0);
        jill.getAddresses().add(new Address("Männi 3"));

        em.merge(jill);

        em.getTransaction().commit();
        em.close();

        List<Address> addresses = findPersonByName("Jill").getAddresses();
        assertThat(addresses.size(), is(2));
        assertThat(addresses.get(0).getStreet(), is("Kuuse 2"));
        assertThat(addresses.get(1).getStreet(), is("Männi 3"));
    }

    @Test
    public void nativeQuery() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Query query = em.createNativeQuery(
                    "TRUNCATE SCHEMA public AND COMMIT");
        query.executeUpdate();

        em.getTransaction().commit();
        em.close();
    }

    private Person findPersonByName(String name) {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Person> query = em.createNamedQuery(
                Person.FIND_BY_NAME, Person.class);
        query.setParameter("name", name);

        List<Person> list = query.getResultList();
        em.close();

        if (list.size() > 1) {
            throw new IllegalStateException(
                    "more than one person named " + name);
        }

        return list.size() == 1 ? list.get(0) : null;
    }

    private List<Person> findAllPersons() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Person> query = em.createNamedQuery(
                Person.FIND_ALL, Person.class);
        return query.getResultList();
    }

    private Person insertPerson(String name) {
        return insertPerson(name, Arrays.<Address>asList());
    }

    private Person insertPerson(String name, List<Address> addresses) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Person person = new Person(name);
        person.setAddresses(addresses);

        em.persist(person);

        em.getTransaction().commit();
        em.close();

        return person;
    }


}
