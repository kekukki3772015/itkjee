package mvc.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mvc.model.Person;

@Repository
public class JpaPersonDao {

    @PersistenceContext
    private EntityManager em;

    public Map<Integer, String> getAgeGroups() {
        LinkedHashMap<Integer, String> map = new LinkedHashMap<Integer, String>();
        map.put(1, "Laps");
        map.put(2, "Täiskasvanu");
        map.put(3, "Pensionär");
        return map;
    }

    @Transactional
    public void save(Person person) {
        if (person.getId() != null) {
            em.merge(person);
        } else {
            em.persist(person);
        }
    }

    public List<Person> findAll() {
        return em.createQuery("select p from Person p",
                Person.class).getResultList();
    }

    @Transactional
    public void delete(Long personId) {
        Person person = em.find(Person.class, personId);
        if (person != null) {
            em.remove(person);
        }
    }

    public Person findById(Long personId) {
        TypedQuery<Person> query = em.createQuery(
                "select p from Person p where p.id = :id", Person.class);
        query.setParameter("id", personId);

        return query.getSingleResult();
    }

}
