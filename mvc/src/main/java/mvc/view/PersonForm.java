package mvc.view;

import java.util.Map;

import mvc.model.Person;

public class PersonForm {

    private Person person;

    private Map<Integer, String> ageGroups;

    private String message;

    private String validateButton;

    private String saveButton;

    public Map<Integer, String> getAgeGroups() {
        return ageGroups;
    }

    public void setAgeGroups(Map<Integer, String> ageGroups) {
        this.ageGroups = ageGroups;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getValidateButton() {
        return validateButton;
    }

    public void setValidateButton(String validateButton) {
        this.validateButton = validateButton;
    }

    public String getSaveButton() {
        return saveButton;
    }

    public void setSaveButton(String saveButton) {
        this.saveButton = saveButton;
    }

    public boolean wasSavePressed() {
        return saveButton != null;
    }

    public boolean wasValidatePressed() {
        return validateButton != null;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
