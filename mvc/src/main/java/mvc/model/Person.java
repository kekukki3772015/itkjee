package mvc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Person {

    @Id
    @SequenceGenerator(name="my_seq", sequenceName="seq1", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="my_seq")
    private Long id;

    @Column(name="first_name")
    private String firstName;
    private String name;

    @Column(name="age_group_id")
    private Integer ageGroupId;

    public Person() {}

    public Person(Long id, String firstName, String name, int ageGroupId) {
        this.setId(id);
        this.firstName = firstName;
        this.name = name;
        this.ageGroupId = ageGroupId;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public Integer getAgeGroupId() {
        return ageGroupId;
    }
    public void setAgeGroupId(Integer ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", firstName=" + firstName + ", ageGroupId=" + ageGroupId + "]";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
