package jdbc;

import org.hsqldb.Server;
import org.hsqldb.persist.HsqlProperties;

public class HsqlDbServerMode {

    public static void main(String[] args) throws Exception {
        HsqlProperties p = new HsqlProperties();
        p.setProperty("server.database.0", "file:~/data/jdbc");
        p.setProperty("server.dbname.0", "srv_mode");
        p.setProperty("server.port", "9001");
        Server server = new Server();
        server.setProperties(p);
        server.start();
    }

}
