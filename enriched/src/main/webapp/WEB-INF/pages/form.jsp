<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<%@ include file="style.jsp"%>
</head>
<body>

  <jsp:include page="menu.jsp" />
  <br /><br />

  <form:form method="POST" modelAttribute="personForm" action="form">
    <c:if test="${not empty message}">
      <div class="messageblock">
        <spring:message code="${message}" />
      </div>
    </c:if>
    <form:errors path="*" id="messageBlock" cssClass="errorblock" element="div" />
    <spring:message code="person.name" /> :
    <form:input path="person.name" />
    <form:errors path="person.name" cssClass="error" />

        <br>

    <spring:message code="person.age" /> :
    <form:input path="person.age" />
    <form:errors path="person.age" cssClass="error" />

        <br>

    <spring:message code="person.code" /> :
    <form:input path="person.code" />
    <form:errors path="person.code" cssClass="error" />

        <br><br>

    <td colspan="3"><input type="submit" value="<spring:message code="button.send"/>"/></td>

  </form:form>

</body>
</html>