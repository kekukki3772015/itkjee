<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style.jsp"%>
</head>
<body>

  <div id="login">

  <jsp:include page="lang.jsp" /><br /><br />

  <c:if test="${not empty error}">
      <spring:message code="${error}"/>
  </c:if>

  <form action="<c:url value="/login" />" method="POST">
    <spring:message code="label.user"/>:
    <input type="text" name="username" value="admin"><br>
    <spring:message code="label.password"/>:
    <input type="password" name="password" value="admin" /><br>
    <input type="submit" value="<spring:message code="button.login"/>" />
  </form>

  </div>

</body>
</html>
