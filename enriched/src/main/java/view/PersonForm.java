package view;

import javax.validation.Valid;
import javax.validation.constraints.*;

import model.Person;

public class PersonForm {

    @NotNull
    @Valid
    private Person person;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

}
