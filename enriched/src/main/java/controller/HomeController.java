package controller;

import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.context.MessageSource;
import org.springframework.security.core.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @Resource
    private MessageSource messages;

    @RequestMapping("/")
    public String home(ModelMap model) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();

        model.addAttribute("message", "Welcome " + userName);

        return "message";
    }

    @RequestMapping("/info")
    public String authenticationInfoExample(ModelMap model, HttpSession session) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        for (GrantedAuthority authority : authentication.getAuthorities()) {
            if (authority.getAuthority().equals("ROLE_ADMIN")) {
                // do admin stuff
            }
        }

        model.addAttribute("message",
                session.getAttribute("SPRING_SECURITY_CONTEXT").toString());

        return "message";
    }

    @RequestMapping("/message")
    public String messageExample(ModelMap model, Locale locale) {

        String[] params = { "p1", "p2" };
        String message = messages.getMessage(
                "message.paramExample", params, locale);

        model.addAttribute("message", message);

        return "message";
    }
}