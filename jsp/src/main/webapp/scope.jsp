<%@ page language="java" contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head></head>

<body>

    <c:set var="var1" value="value1" scope="page" />
    <c:set var="var2" value="value2" scope="request" />
    <c:set var="var3" value="value3" scope="session" />
    <c:set var="var4" value="value4" scope="application" />

    var1: <c:out value='${pageScope.var1}' /> <br/>
    var2: <c:out value='${requestScope.var2}' /> <br/>
    var3: <c:out value='${sessionScope.var3}' /> <br/>
    var4: <c:out value='${applicationScope.var4}' /> <br/>

    var1: <c:out value='${pageScope["var1"]}' /><br/>
    var2: <c:out value='${requestScope["var2"]}' /><br/>
    var3: <c:out value='${sessionScope["var3"]}' /><br/>
    var4: <c:out value='${applicationScope["var4"]}' /><br/>

</body>